import domain.RPGCharacter
import domain.MeleeFighter
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertFalse
import org.junit.Test

class HealCharacterTest {

    private lateinit var dopaCharacter: RPGCharacter
    private lateinit var topaCharacter: RPGCharacter
    private val fighterClass = MeleeFighter()

@Test
    fun `Can not heal dead characters`() {
        givenADeadCharacter()
        whenHeals()
        thenTheCharacterRemainsDead()
    }

    @Test
    fun `Can not over heal above 1000`() {
        givenACharacter()
        whenOverHeals()
        thenHealthDoesNotChange()
    }

    @Test
    fun `Can not heal enemies`() {
        givenTwoCharactersWithHalfHealth()
        whenHealsAnEnemy()
        thenEnemysHealthDoesNotChange()
    }

    @Test
    fun `can heal allies`() {
        givenTwoAlliesAndOneOfThemWithMiddleHealth()
        whenTryToHeal()
        thenHealIsDone()
    }

    private fun thenHealIsDone() {
        var expectedHealth = 800
        assertEquals(expectedHealth,topaCharacter.getActualHealth())
    }

    private fun whenTryToHeal() {
        dopaCharacter.heal(topaCharacter)
    }

    private fun givenTwoAlliesAndOneOfThemWithMiddleHealth() {
        givenTwoCharacters()
        dopaCharacter.joinFaction("Curanderos")
        topaCharacter.joinFaction("Curanderos")
        topaCharacter.setHealth(500)
    }

    private fun givenTwoCharacters() {
        givenACharacter()
        topaCharacter = RPGCharacter(fighterClass)
    }

    private fun givenACharacter() {
        dopaCharacter = RPGCharacter(fighterClass)
    }

    private fun givenADeadCharacter() {
        givenACharacter()
        dopaCharacter.setHealth(0)
    }

    private fun givenTwoCharactersWithHalfHealth() {
        givenACharacter()
        dopaCharacter.setHealth(500)
        dopaCharacter.joinFaction("Insectos")
        topaCharacter = RPGCharacter(fighterClass)
        topaCharacter.setHealth(500)
        topaCharacter.joinFaction("Microbios")
    }

    private fun whenHeals() {
        dopaCharacter.heal(dopaCharacter)
    }

    private fun whenOverHeals() {
        dopaCharacter.heal(dopaCharacter)
    }

    private fun whenHealsAnEnemy() {
        dopaCharacter.heal(topaCharacter)
    }

    private fun thenTheCharacterRemainsDead() {
        assertFalse(dopaCharacter.isAlive)
    }

    private fun thenHealthDoesNotChange() {
        assertEquals(1000,dopaCharacter.getActualHealth())
    }

    private fun thenEnemysHealthDoesNotChange() {
        assertEquals(500,topaCharacter.getActualHealth())
    }
}