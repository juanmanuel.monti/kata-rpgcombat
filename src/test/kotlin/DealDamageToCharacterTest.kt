import domain.RPGCharacter
import domain.RangedFighter
import org.junit.Assert.*
import org.junit.Test

class DealDamageToCharacterTest {

    private lateinit var dopaCharacter: RPGCharacter
    private lateinit var topaCharacter: RPGCharacter
    private val fighterClass = RangedFighter()
    private val enemyDistance = 2
    private val outOfRangeEnemyDistance= 21

    @Test
    fun `decrease enemy's health`() {
        givenTwoCharacters()
        whenDealDamage()
        thenEnemysHealthIsDecreased()
    }

    @Test
    fun `kill an opponent with damage bigger than health`() {
        givenOneAttackerAndAnEnemyAlmostDead()
        whenDealDamage()
        thenOpponentisDead()
    }

    @Test
    fun `can not damage themselves`() {
        givenACharacter()
        whenDealDamageHimself()
        thenHealthDoesNotChange()
    }

    @Test
    fun `deal increased damage to enemies 5 or more levels lower`() {
        givenTwoCharactersWithFiveLevelsOfDifference()
        whenHigherLevelCharacterDealDamage()
        thenEnemyReceiveIncreasedDamage()
    }

    @Test
    fun `deal decreased damage to enemies 5 or more levels higher`() {
        givenTwoCharactersWithFiveLevelsOfDifference()
        whenLowerLevelCharacterDealDamage()
        thenEnemyReceibeDecreasedDamage()
    }

    @Test
    fun `can not deal damage to enemies out of range`() {
        givenTwoCharacters()
        whenTryToDealDamageOutOfRange()
        thenDamageIsNotDealt()
    }

    @Test
    fun `can not deal damage to allies`() {
        givenTwoAllies()
        whenTryToDealDamageToAnAlly()
        thenDamageIsNotDealt()
    }

    private fun whenTryToDealDamageToAnAlly() {
        dopaCharacter.dealDamage(topaCharacter,enemyDistance)
    }

    private fun givenTwoAllies() {
        givenTwoCharacters()
        dopaCharacter.joinFaction("Friends")
        topaCharacter.joinFaction("Friends")
    }

    private fun givenTwoCharacters() {
        givenACharacter()
        topaCharacter = RPGCharacter(fighterClass)
    }

    private fun givenACharacter() {
        dopaCharacter = RPGCharacter(fighterClass)
    }

    private fun givenOneAttackerAndAnEnemyAlmostDead() {
        givenTwoCharacters()
        topaCharacter.setHealth(10)
    }

    private fun givenTwoCharactersWithFiveLevelsOfDifference() {
        givenTwoCharacters()
        dopaCharacter.setNewLevel(6)
    }

    private fun whenDealDamage() {
        dopaCharacter.dealDamage(topaCharacter,enemyDistance)
    }

    private fun whenDealDamageHimself() {
        dopaCharacter.dealDamage(dopaCharacter,enemyDistance)
    }

    private fun whenHigherLevelCharacterDealDamage() {
        dopaCharacter.dealDamage(topaCharacter,enemyDistance)
    }

    private fun whenLowerLevelCharacterDealDamage() {
        topaCharacter.dealDamage(dopaCharacter,enemyDistance)
    }

    private fun whenTryToDealDamageOutOfRange() {
        dopaCharacter.dealDamage(topaCharacter,outOfRangeEnemyDistance)
    }

    private fun thenEnemysHealthIsDecreased() {
        var remainingTopasHealth = topaCharacter.getActualHealth()
        assertEquals(900, remainingTopasHealth)
    }

    private fun thenOpponentisDead() {
        assertFalse(topaCharacter.isAlive)
    }

    private fun thenHealthDoesNotChange() {
        assertEquals(1000,dopaCharacter.getActualHealth())
    }

    private fun thenEnemyReceiveIncreasedDamage() {
        assertEquals(850,topaCharacter.getActualHealth())
    }

    private fun thenEnemyReceibeDecreasedDamage() {
        assertEquals(950,dopaCharacter.getActualHealth())
    }

    private fun thenDamageIsNotDealt() {
        assertEquals(1000,topaCharacter.getActualHealth())
    }


}