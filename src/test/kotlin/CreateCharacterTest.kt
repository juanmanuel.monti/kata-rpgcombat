import domain.RPGCharacter
import domain.MeleeFighter
import junit.framework.Assert.assertEquals
import org.junit.Test


class CreateCharacterTest {
    private lateinit var character: RPGCharacter
    private val fighterClass = MeleeFighter()

    @Test
    fun `has initial health`() {
        whenCreates()
        thenCreatedCharacterHasInitialHealth()
    }

    @Test
    fun `has a level`() {
        whenCreates()
        thenCreatedCharacterHasInitialLevel()
    }

    @Test
    fun `starts alive`() {
        whenCreates()
        thenCreatedCharacterStartsAlive()
    }

    private fun whenCreates(){
        character = RPGCharacter(fighterClass)
    }

    private fun thenCreatedCharacterHasInitialHealth(){
        val initialHealth = character.getActualHealth()
        assertEquals(EXPECTED_HEALTH,initialHealth)
    }

    private fun thenCreatedCharacterHasInitialLevel(){
        val initialLevel = character.level
        assertEquals(EXPECTED_LEVEL,initialLevel)
    }

    private fun thenCreatedCharacterStartsAlive(){
        val isAlive = character.isAlive
        assertEquals(true,isAlive)
    }

    companion object{
        const val EXPECTED_HEALTH = 1000
        const val EXPECTED_LEVEL = 1
    }
}

