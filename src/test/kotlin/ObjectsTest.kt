import domain.*
import junit.framework.Assert.assertEquals
import org.junit.Test

class ObjectsTest {

    @Test
    fun `can damage objects`() {

       val dopaCharacter = RPGCharacter(MeleeFighter())
       val tree = RPGObject()

        dopaCharacter.dealDamage(tree,2)

        assertEquals(1900,tree.getActualHealth())
    }

    @Test
    fun `can not damage objects out of range`() {
        val dopaCharacter = RPGCharacter(MeleeFighter())
        val tree = RPGObject()

        dopaCharacter.dealDamage(tree,3)

        assertEquals(2000,tree.getActualHealth())
    }

    @Test
    fun `can not heal objects`() {
        val dopaCharacter = RPGCharacter(RangedFighter())

        val tree = RPGObject()

        //dopaCharacter.heal(tree) deberia poder interactuar con el objeto? como valido que no se puede curar?
    }

    @Test
    fun `objects can not deal damage`() {
        val dopaCharacter = RPGCharacter(RangedFighter())
        val tree = RPGObject()

        //tree.dealDamage(dopaCharacter)
    }

    @Test
    fun `objects do not belong to factions`() {
        val tree = RPGObject()
        assertEquals(setOf<String>(),tree.factions)
    }
}