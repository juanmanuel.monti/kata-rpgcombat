import domain.RPGCharacter
import domain.MeleeFighter
import domain.RangedFighter
import junit.framework.Assert.assertEquals
import org.junit.Test

class FactionTest {

    private lateinit var dopaCharacter: RPGCharacter
    private lateinit var nicoCharacter: RPGCharacter


    @Test
    fun `newly created characters belong to no faction`() {
        whenCreates()
        thenCreatedCharacterShouldBelongToNoFaction()
    }

    @Test
    fun `join a faction`() {
        givenACreatedCharacter()
        whenJoinAFaction()
        thenIsMemberOfAFaction()
    }

    @Test
    fun `join more than one faction`() {
        givenACreatedCharacter()
        whenJoinTwoFactions()
        thenHasTwoAlliances()
    }

    @Test
    fun `has an alliance`() {
        whenTwoCreatedCharactersWithTheSameFaction()
        thenTheyAreAllies()
    }

    @Test
    fun `leave a faction`() {
        givenACreatedCharacter()
        dopaCharacter.joinFaction("Order")
        dopaCharacter.joinFaction("Chaos")
        dopaCharacter.leaveFaction("Order")
        dopaCharacter.leaveFaction("Chaos")
        assertEquals(false, dopaCharacter.isMemberOfTheFaction("Order"))
        assertEquals(false, dopaCharacter.isMemberOfTheFaction("Chaos"))
    }

    private fun thenTheyAreAllies() {
        val isNicoAlly = dopaCharacter.isAlly(nicoCharacter)
        val isDopaAlly = nicoCharacter.isAlly(dopaCharacter)
        assertEquals(true,isDopaAlly&&isNicoAlly)
    }

    private fun givenACreatedCharacter() {
        dopaCharacter = RPGCharacter(MeleeFighter())
    }

    private fun whenTwoCreatedCharactersWithTheSameFaction() {
        givenACreatedCharacter()
        dopaCharacter.joinFaction("Order")
        dopaCharacter.joinFaction("Chaos")
        dopaCharacter.joinFaction("Nightcrawlers")
        nicoCharacter = RPGCharacter(RangedFighter())
        nicoCharacter.joinFaction("Butchers")
        nicoCharacter.joinFaction("Chaos")
    }

    private fun whenCreates() {
        dopaCharacter = RPGCharacter(MeleeFighter())
    }

    private fun whenJoinAFaction() {
        dopaCharacter.joinFaction("Order")
    }

    private fun whenJoinTwoFactions() {
        dopaCharacter.joinFaction("Order")
        dopaCharacter.joinFaction("Chaos")
    }

    private fun thenCreatedCharacterShouldBelongToNoFaction() {
        val isInAFaction = dopaCharacter.isInAFaction()
        assertEquals(false,isInAFaction)
    }

    private fun thenIsMemberOfAFaction() {
        val hasAFaction = dopaCharacter.isMemberOfTheFaction("Order")
        assertEquals(true, hasAFaction)
    }

    private fun thenHasTwoAlliances() {
        val hasAnAlliance = dopaCharacter.isMemberOfTheFaction("Order")
        val hasAnotherAlliance = dopaCharacter.isMemberOfTheFaction("Chaos")
        assertEquals(true, hasAnAlliance&&hasAnotherAlliance)
    }
}