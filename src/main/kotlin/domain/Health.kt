package domain

import java.lang.Integer.max
import java.lang.Integer.min

class Health(var amount:Int){

    //var amount: Int = MAX_HEALTH

    fun increase(healingAmount: Int){
        amount = min(MAX_HEALTH,amount+healingAmount)
    }

    fun decrease(damageAmount: Int){
        amount = max(MIN_HEALTH,amount-damageAmount)
    }

    companion object {
        const val MAX_HEALTH = 1000
        const val MIN_HEALTH = 0
    }
}