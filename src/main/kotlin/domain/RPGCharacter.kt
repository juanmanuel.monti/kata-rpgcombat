package domain

class RPGCharacter(private val combatClass: FighterClass):RPGTarget{

    private val health = Health(1000)
    var level = INITIAL_LEVEL
    private val damage = Damage()
    val isAlive get() = health.amount > 0
    override var factions: MutableSet<String> = mutableSetOf()

    fun dealDamage(target: RPGTarget, distanceToTarget: Int){
          if (canDamage(target,distanceToTarget)){
                target.receiveDamage(this)
            }
        }

    override fun receiveDamage(from: RPGCharacter){
        val trueDamage = damage.calculate(from.level,level)
        health.decrease(trueDamage)
    }

    private fun canDamage(target: RPGTarget, distanceToEnemy: Int):Boolean{
        val attackerRange = this.getCombatRange()
        return (!isAlly(target)) && isInRange(attackerRange,distanceToEnemy)
    }

    fun heal(character:RPGCharacter){
        if (canHeal(character))
            character.receiveHealing(HEALING_AMOUNT)
    }

    private fun canHeal(character: RPGCharacter) = character.isAlive && isAlly(character)

    private fun receiveHealing(healingAmount: Int){
        health.increase(healingAmount)
    }

    fun setNewLevel(newLevel:Int){
        level = newLevel
    }

    fun setHealth(newHealth:Int){
        health.amount = newHealth
    }

    override fun getActualHealth():Int{
        return health.amount
    }

    private fun getCombatRange():Int{
        return combatClass.attackRange
    }

    private fun isInRange(attackerRange: Int, distanceToEnemy:Int ):Boolean{
        return (attackerRange>=distanceToEnemy)
    }

    fun isInAFaction():Boolean = factions.any()

    fun isAlly(target: RPGTarget): Boolean {

        if(target == this) return true
        var characterFactions = getFactions(target)
        return characterFactions
                .any{ factions.contains(it) }
    }

    fun isMemberOfTheFaction(factionName:String):Boolean{
        return factions.contains(factionName)
    }

    fun joinFaction(newFaction: String) {
        factions.add(newFaction)
    }

    fun leaveFaction(actualFaction: String) {
        factions.remove(actualFaction)
    }

    private fun getFactions(target: RPGTarget): Set<String> {
        return target.factions
    }

    fun getDamageAmount():Int = damage.amount

    companion object{
        const val INITIAL_LEVEL = 1
        const val HEALING_AMOUNT = 300
    }
}