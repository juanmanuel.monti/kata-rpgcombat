package domain

class Damage{
    val amount = 100

    fun calculate(attackerLevel: Int,targetLevel: Int):Int{
        val levelModifier = getLevelModifier(attackerLevel,targetLevel)
        return (amount*levelModifier).toInt()
    }

    fun getLevelModifier(attackerLevel:Int, enemyLevel: Int):Double{
        return when{
            (attackerLevel-enemyLevel) >= LEVEL_DAMAGE_MARGIN -> 1.5
            (enemyLevel-attackerLevel) >= LEVEL_DAMAGE_MARGIN -> 0.5
            else -> 1.0
        }
    }



    companion object{
        const val LEVEL_DAMAGE_MARGIN = 5
    }
}