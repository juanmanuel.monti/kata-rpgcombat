package domain

class RPGObject : RPGTarget {

    private val health = Health(2000)
    override var factions: MutableSet<String> = mutableSetOf()

    override fun receiveDamage(from: RPGCharacter) {
        val damageAmount = from.getDamageAmount()
        health.decrease(damageAmount)
    }

    override fun getActualHealth() =health.amount
}