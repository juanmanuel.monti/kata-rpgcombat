package domain

interface RPGTarget {
    fun receiveDamage(from: RPGCharacter)
    fun getActualHealth():Int
    var factions:MutableSet<String>
}